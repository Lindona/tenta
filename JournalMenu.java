import java.util.Scanner;

public class JournalMenu extends Menu {

	@Override
	public void printMenu() {
		System.out.println("1. S�k journal");
		System.out.println("2. Visa samtliga journaler f�r att specifik kontakt");
		
	}

	@Override
	public void choice(Scanner scan) {
		String select = "";
		
		
		do {
			select = scan.nextLine();
			switch(select) {
			case "1":
				System.out.println("Ange s�kord: "); 
				break;
			case "2":
				System.out.println("H�r visas journaler f�r en viss kund.");
				break;
			case "0":
				Menu.main(null);
				break;
			default:
				System.out.println("Invalid option. Try again.");
				break;
			}
		} while(true);
		
	}

}