import java.util.Scanner;

public abstract class Menu {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		MainMenu startMenu = new MainMenu();
		startMenu.printMenu();
		startMenu.choice(scan);	
		scan.close();	

	}
	
	public abstract void printMenu();
	
	public abstract void choice(Scanner scan);
}