import java.util.Scanner;

public class ContactMenu extends Menu {

	@Override
	public void printMenu() {
		System.out.println("-= Contact menu =-");
		System.out.println("1. Skapa kontakt");
		System.out.println("2. Lista kontakter");
		System.out.println("3. Ta bort kontakt");	
		System.out.println("0. Return to main menu");
	}

	@Override
	public void choice(Scanner scan) {
		String select = "";
		
		do {
			select = scan.nextLine();
			switch(select) {
			case "1":
				System.out.println("Skapar ny kontakt.........");
				break;
			case "2":
				System.out.println("Visar tydlig lista �ver kontakter");
				break;
			case "3":
				System.out.println("ange vem som f�tt sparken:");
				break;
			case "0":
				Menu.main(null);
				break;
			default:
				System.out.println("Invalid option. Try again.");
				break;
			}
		} while(true);
	}
}