import java.util.Scanner;

public class MainMenu extends Menu {

	@Override
	public void printMenu() {
		System.out.println("-= Huvudmeny =-");
		System.out.println("1. Kontakter");
		System.out.println("2. M�ten");
		System.out.println("3. Journal");
		System.out.println("0. End program");
	}

	@Override
	public void choice(Scanner scan) {
		String select = "";		
		do {
			select = scan.nextLine();
			switch(select) {
			case "1":
				ContactMenu cMenu = new ContactMenu();
				cMenu.printMenu();
				cMenu.choice(scan);
				break;
			case "2":
				MeetingMenu mMenu = new MeetingMenu();
				mMenu.printMenu();
				mMenu.choice(scan);
				break;
			case "3":
				JournalMenu jMenu = new JournalMenu();
				jMenu.printMenu();
				jMenu.choice(scan);
				break;
			case "0":
				System.out.println("Bye bye");
				System.exit(0);
				break;
			default:
				System.out.println("Invalid option. Try again.");
				break;
			}
		} while(true);
		
		
	}

}

/*
	public void newContact(Scanner scan){
		String fName, lName, mail;
		
		System.out.println("fName?");
		fName = scan.nextLine();
		System.out.println("lName?");
		lName = scan.nextLine();
		System.out.println("mail?");
		mail = scan.nextLine();
		
		Person newPerson = new Person (fName, lName, mail);
		contacts.add(newPerson);
	}
*/
