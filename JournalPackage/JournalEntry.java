package JournalPackage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Scanner;

public class JournalEntry {
	
	public void Journal() throws IOException {
		
		//create a folder to hold all journals
		File folder = new File("Journal");
		if(folder.exists()) {	
		}else {
			folder.mkdirs();
		}
		
		//create a file according to date 
		Calendar cal = Calendar.getInstance();
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter Headline");
		String journalHead = input.nextLine();
	
		File journal = new File("Journal\\" + cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH) 
					+ "-" + cal.get(Calendar.DATE) + " " + journalHead + ".txt");
		journal.createNewFile();
		
		//add attending contacts
		System.out.println("Wich client attended the meeting:");
		String journalClient = input.nextLine();
		
		//write to file		
		System.out.println("Enter text");
		String journalBody = input.nextLine();
		FileWriter fw = new FileWriter(journal);
		fw.write(journalClient + "\n" + journalBody);
		
		input.close();
		fw.close();
	}

}
