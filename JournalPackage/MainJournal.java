package JournalPackage;

import java.io.IOException;
import java.util.Scanner;

public class MainJournal {

	public static void main(String[] args) throws IOException {
		
		Scanner scan = new Scanner(System.in);
		
		JournalEntry j = new JournalEntry();
		FindEntry f = new FindEntry();
		
		System.out.println("1. Write a Journal entry");
		System.out.println("2. View all Journals");
		
		String option;
		option = scan.nextLine();
		
		if(option.equals("1")) {
			j.Journal();
		}else {
		f.openFile();
		}
		
		scan.close();

	}

}
