package contactPackage;

public class Contact {

	public String fName;
	public String lName;
	public String adress;
	public String phoneNr;
	public String mailAdress;
	public String personNr;
	
	public Contact(String fName, String lName, String adress, String phoneNr, String mailAdress, String personNr) {
		this.fName = fName;
		this.lName = lName;
		this.adress = adress;
		this.phoneNr = phoneNr;
		this.mailAdress = mailAdress;
		this.personNr = personNr;
	}
	public Contact() {
		fName = "Nisse";
		lName = "Nilsson";
		adress = "Fejkgatan 123";
		phoneNr = "123456";
		mailAdress = "nisse@default.com";
		personNr = "000000-0000";
	}
}
