package contactPackage;
import java.util.ArrayList;
import java.util.Scanner;

public class ChangeContact {

	Scanner changeC = new Scanner(System.in);
	Scanner sc = new Scanner(System.in);

	public void showContacts(ArrayList<Contact> arrayContactList) {

		System.out.println("Vilken vill du �ndra?");
		for (Contact person : arrayContactList) {
			System.out.println(person.fName + " " + person.lName);
			System.out.println();
		}
		change(arrayContactList);
	}

	public void change(ArrayList<Contact> array) {
		String ch = changeC.nextLine();
		for (Contact person : array) {
			String f�r = person.fName;
			String efter = person.lName;

			if (f�r == null || efter == null) {
				System.out.println("Inga kontakter inlagda");
			}
			if (f�r.contains(ch) || efter.contains(ch)) {
				int i = array.indexOf(person);
				System.out.println("Ny kontakt");
				System.out.println("Personens f�rnamn: ");
				String f�rnamn = sc.nextLine();
				System.out.println("Personens efternamn: ");
				String efternamn = sc.nextLine();
				System.out.println("Personens adress: ");
				String adress = sc.nextLine();
				System.out.println("Personens telefonnr: ");
				String tel = sc.nextLine();
				System.out.println("Personens mailadress: ");
				String mail = sc.nextLine();
				System.out.println("Personens personnr: ");
				String persnr = sc.nextLine();

				Contact cont = new Contact(f�rnamn, efternamn, adress, tel, mail, persnr);
				array.set(i, cont);
				System.out.println("Kontakten redigerad");

			}
		}
	}
}
